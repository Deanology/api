﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Model;

namespace Todo.Interfaces
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> GetMovies();
        Movie GetMovie(int id);
        void UpdateMovie(Movie movie);
        void PostMovie(Movie movie);
        void DeleteMovie(Movie movie);
        bool MovieExists(int id);
        void Save();
    }
}

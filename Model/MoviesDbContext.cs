﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Todo.Model
{
    public class MoviesDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product2> Product2 { get; set; }
        public MoviesDbContext(DbContextOptions<MoviesDbContext> options): base(options)
        {

        }
        /*protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Movie>().ToTable("Movie");
            //FLUENT API FOR CATEGORIES
            modelBuilder.Entity<Category>().ToTable("Categories");
            modelBuilder.Entity<Category>().HasKey(p => p.Id);
            modelBuilder.Entity<Category>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<Category>().Property(p => p.Name).IsRequired().HasMaxLength(30);
            modelBuilder.Entity<Category>().HasMany(p => p.Products).WithOne(p => p.Category).HasForeignKey(p=> p.CategoryId);


            //FLUENT API FOR PRODUCT2
            modelBuilder.Entity<Product2>().ToTable("Products");
            modelBuilder.Entity<Product2>().HasKey(p => p.Id);
            modelBuilder.Entity<Product2>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<Product2>().Property(p => p.Name).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<Product2>().Property(p => p.QuantityInPackage).IsRequired();
            modelBuilder.Entity<Product2>().Property(p => p.UnitOfMeasurement).IsRequired();
        }*/
    }
}

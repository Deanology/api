﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Interfaces;
using Todo.Model;

namespace Todo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryRepository _cat;
        public CategoriesController(ICategoryRepository cat)
        {
            _cat = cat;
        }
        //GET: api/categories
        [HttpGet]
        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await _cat.AllCategories();
        }
    }
}

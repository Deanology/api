﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Interfaces;
using Todo.Model;

namespace Todo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : Controller
    {
        private ILogger _logger;
        private IProductRepository _productRepository;
        public ProductController(ILogger<ProductController> logger, IProductRepository productRepository)
        {
            _logger = logger;
            _productRepository = productRepository;
        }
        [HttpGet("/api/product")]
        public ActionResult<List<Product>> GetProducts()
        {
            return _productRepository.GetProducts();
        }
        [HttpPost("/api/product")]
        public ActionResult<Product> AddProduct(Product product)
        {
            _productRepository.AddProduct(product);
            return product;
        }
        [HttpPut("/api/product/{id}")]
        public ActionResult<Product> UpdateProduct(String id,Product product)
        {
            _productRepository.UpdateProduct(id, product);
            return product;
        }
        [HttpDelete("/api/product/{id}")]
        public ActionResult<string> DeleteProduct(String id)
        {
            _productRepository.DeleteProduct(id);
            return id;
        }
    }
}

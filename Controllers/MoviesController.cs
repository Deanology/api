﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Interfaces;
using Todo.Model;

namespace Todo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository _movie;
        private ILogger _logger;
        public MoviesController(IMovieRepository movie, ILogger<ProductController> logger)
        {
            _movie = movie;
            _logger = logger;
        }
        //GET: api/Movies
        [HttpGet]
        public IEnumerable<Movie> GetMovies()
        {
            return _movie.GetMovies();
        }
        //GET: api/Movies/5
        [HttpGet("{id}")]
        public ActionResult<Movie> GetMovie(int id)
        {
            var movie = _movie.GetMovie(id);
            if (movie == null)
            {
                return NotFound();
            }
            return movie;
        }
        //PUT: api/Movies/5
        [HttpPut("{id}")]
        public IActionResult PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }
            _movie.UpdateMovie(movie);
            try
            {
                _movie.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_movie.MovieExists(id))
                {
                    return NotFound();
                }
                throw;
            }
            return NoContent();
        }
        //DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public ActionResult<Movie> DeleteMovie(int id)
        {
            var movie = _movie.GetMovie(id);
            if (movie == null)
            {
                return NotFound();
            }
            _movie.DeleteMovie(movie);
            _movie.Save();
            return movie;
        }
        //POST: api/Movies
        [HttpPost]
        public ActionResult<Movie> PostMovie(Movie movie)
        {
            _movie.PostMovie(movie);
            _movie.Save();
            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }
    }
}

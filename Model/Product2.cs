﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Model.Enum;

namespace Todo.Model
{
    public class Product2
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public short QuantityInPackage { get; set; }
        public EUnitOfMeasurement UnitOfMeasurement { get; set; }
        //a product has one, and only one category 
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Todo.Interfaces;
using Todo.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Todo.Services
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly MoviesDbContext _context;
        public CategoryRepository(MoviesDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Category>> AllCategories()
        {
            return await _context.Categories.ToListAsync();
        }
    }
}

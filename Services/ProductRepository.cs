﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Interfaces;
using Todo.Model;

namespace Todo.Services
{
    public class ProductRepository : IProductRepository
    {
        private List<Product> _products;
        public ProductRepository()
        {
            _products = new List<Product>();
        }
        public Product AddProduct(Product product)
        {
            _products.Add(product);
            return product;
        }

        public string DeleteProduct(string id)
        {
            for (var index = _products.Count - 1; index >= 0; index--)
            {
                if (_products[index].ID == id)
                {
                    _products.RemoveAt(index);
                }
            }
            return id;
        }

        public List<Product> GetProducts()
        {
            return _products;
        }

        public Product UpdateProduct(string id, Product product)
        {
            for (var index = _products.Count -1;  index >=0; index--)
            {
                if (_products[index].ID == id)
                {
                    _products[index] = product;
                }
            }
            return product;
        }
    }
}

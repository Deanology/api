﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Model;

namespace Todo.Interfaces
{
    public interface IProductRepository
    {
        List<Product> GetProducts();
        Product AddProduct(Product product);
        Product UpdateProduct(string id, Product product);
        string DeleteProduct(string id);
    }
}

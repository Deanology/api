﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Model;

namespace Todo.Interfaces
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> AllCategories();
    }
}

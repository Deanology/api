﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.Model
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        // a category has many products
        public ICollection<Product2> Products { get; set; }
    }
}

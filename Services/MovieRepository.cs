﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Interfaces;
using Todo.Model;

namespace Todo.Services
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MoviesDbContext _context;
        public MovieRepository(MoviesDbContext context)
        {
            _context = context;
        }
        public void DeleteMovie(Movie movie)
        {
            _context.Movies.Remove(movie);
        }
        public Movie GetMovie(int id)
        {
            return _context.Movies.Find(id);
        }

        public IEnumerable<Movie> GetMovies()
        {
            return _context.Movies.ToList();
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        public void PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
        }

        public void UpdateMovie(Movie movie)
        {
            _context.Entry(movie).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
